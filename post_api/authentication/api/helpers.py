import requests
import clearbit
from datetime import timedelta

from django.utils import timezone
from django.conf import settings

from rest_framework.authtoken.models import Token


def expires_in(token):
    time_elapsed = timezone.now() - token.created
    left_time = timedelta(
        seconds=settings.TOKEN_EXPIRED_AFTER_SECONDS
        ) - time_elapsed
    return left_time


def is_token_expired(token):
    return expires_in(token) < timedelta(seconds=0)


def token_expire_handler(token):
    is_expired = is_token_expired(token)
    if is_expired:
        token.delete()
        token = Token.objects.create(user=token.user)
    return is_expired, token


def verif_hunter_email(email):
    url = 'https://api.hunter.io/v2/email-verifier?email={}&api_key={}'.format(
        email,
        settings.API_HUNTER_KEY
        )
    response = requests.get(url)
    return response.status_code


def clearbit_data(email):
    clearbit.key = settings.KEY_CLEARBIT
    try:
        person = clearbit.Person.find(email=email, stream=True)
        if person:
            return person
    except requests.exceptions.HTTPError:
        pass