from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.decorators import permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated

from django.core.exceptions import ObjectDoesNotExist

from authentication.models import User
from .helpers import token_expire_handler
from .permissions import IsOwnerOrReadOnly
from .serializers import UserSerializer, UserLoginSerializer


class UserApiHandler(APIView):
    serializer_class = UserSerializer
    permission_classes = [IsOwnerOrReadOnly]

    def get(self, request, id=None):
        if id:
            try:
                user = User.objects.get(id=id)
                serializer = self.serializer_class(user)
            except ObjectDoesNotExist:
                return Response(
                    {'error': 'This user is not exist!'},
                    status=status.HTTP_400_BAD_REQUEST
                    )
        else:
            users = User.objects.all()
            serializer = self.serializer_class(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id=None):
        try:
            user = User.objects.get(id=id)
            self.check_object_permissions(self.request, user)
            user.delete()
            return Response(
                {'success': 'Your user was deleted'},
                status=status.HTTP_200_OK
                )
        except ObjectDoesNotExist:
            return Response(
                {'error': 'This user is not exist!'},
                status=status.HTTP_400_BAD_REQUEST
                )


@permission_classes((AllowAny,))
class TokenHandlerApi(APIView):
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
                )
        try:
            user = User.objects.get(email=serializer.data.get('email'))
        except ObjectDoesNotExist:
            return Response(
                {'error': 'Invalid credentials'},
                status=status.HTTP_400_BAD_REQUEST
                )

        if user.check_password(serializer.data.get('password')):
            token, created = Token.objects.get_or_create(user=user)
            is_expired, token = token_expire_handler(token)
            return Response(
                {'Token': token.key},
                status=status.HTTP_201_CREATED
                )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Logout(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        self.request.user.auth_token.delete()
        return Response(
            {'success': 'You are logout!'},
            status=status.HTTP_200_OK
            )