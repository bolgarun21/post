from django.urls import path
from .handlers import UserApiHandler


urlpatterns = [
    path('user', UserApiHandler.as_view(), name='user'),
    path('user/<int:id>', UserApiHandler.as_view()),
]