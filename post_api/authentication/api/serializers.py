from rest_framework import serializers

from django.core.exceptions import ValidationError

from .helpers import verif_hunter_email, clearbit_data
from authentication.models import User


class UserSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(max_length=128)
    full_name = serializers.CharField(
        max_length=120,
        default=None,
        required=False,
        allow_blank=True
        )
    site = serializers.EmailField(
        default=None,
        required=False,
        allow_blank=True
        )
    country = serializers.CharField(
        max_length=120,
        default=None,
        required=False,
        allow_blank=True
        )
    created_at = serializers.DateTimeField(read_only=True)

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise ValidationError("Email already exists.")
        if verif_hunter_email(value) != 200:
            raise ValidationError("Email is not correct.")
        return value

    def validate_password(self, value):
        if len(value) < 8:
            raise ValidationError(
                "Password should be atleast 8 characters long."
            )
        return value

    def create(self, validated_data):
        data = clearbit_data(validated_data['email'])
        if data and not validated_data['full_name']:
            validated_data['full_name'] = data['name']['fullName']
        if data and not validated_data['site']:
            validated_data['site'] = data['site']
        if data and not validated_data['country']:
            validated_data['country'] = data['geo']['country']

        user = User.objects.create(
            email=validated_data['email'],
            password=validated_data['password'],
            full_name=validated_data['full_name'],
            site=validated_data['site'],
            country=validated_data['country']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required=True,
        label="Email Address"
    )
    password = serializers.CharField(
        required=True,
        style={'input_type': 'password'}
    )

    def validate_email(self, value):
        if value:
            return value
        raise ValidationError("Invalid email.")

    def validate_password(self, value):
        if value:
            return value
        raise ValidationError("Invalid password.")