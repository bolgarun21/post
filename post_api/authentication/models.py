from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from authentication.managers import UserManager
from django.contrib.auth.models import PermissionsMixin


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=128)
    full_name = models.CharField(max_length=120, blank=True, null=True)
    site = models.EmailField(blank=True, null=True)
    country = models.CharField(max_length=120, blank=True, null=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        db_table = ('user')

    def __str__(self):
        return self.email
