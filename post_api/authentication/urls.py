from django.urls import path
from authentication.api.handlers import TokenHandlerApi, Logout


urlpatterns = [
    path('token', TokenHandlerApi.as_view(), name='token'),
    path('token_out', Logout.as_view(), name='token_out'),
]