## Authorization

You must login to get started. If you don't login, you will be notified that you are not authorized.
```bash
POST method
```
Follow this path http://test.com/auth/token.

Replace the fields in parameters "email" and "password" on your own email and password:
```bash
                       {'email': 'exampl@test.com', 'password': 'test987654'}

```
After using the POST method, the above URL and parameters (yours email and password) will be created token.
A token is a secret key without is impossible to view, create, edit or delete objects or logout.
```bash

                       Exampl:  {'Authorization': Token 092e00e4893d42cce7dbfddfc3a2f9e9508743d3}

```

## Unauthorization
```bash
GET method
```
Follow this path http://test.com/auth/token_out

This method is not available unless you are authorized.

Going url position using the GET method and passing a Token, will be delete Token and the session will end. You will be unauthorized.


## Record and view post
Using methods POST, GET, http://test.com/api/v1/post and Token

```bash
GET method
```
GET: return the list with all posts or if you follow http://test.com/api/v1/post/{post_id} you get one post wich you want
```bash
POST method
```
POST: to create new post with current date

Replace the field in parameter:
```bash
                        {'title': 'Title exampl', 'content': 'Content exampl'}
```
These methods are not available to you if you are not authorized.


## Create, view, delete users
Using methods POST, GET, DELETE http://test.com/api/v1/user

```bash
GET method
```
If GET request will accept user_id then he return user which you need, like:

http://test.com/api/v1/user/1 - will return an user that has id 1

http://test.com/api/v1/user/2 - will return an user that has id 2

If GET request not accept user_id then he return all users from database

```bash
POST method
```

* http://test.com/api/v1/user

* method POST

* Token ({'Authorization': Token 092e00e4893d42cce7dbfddfc3a2f9e9508743d3})


```bash
          Exampl value: {
                          "email": "soll@test.com",
                          "password":"dsdaa21fg",
                          "full_name":"Soll Pat",
                          "site":"test@test.com",
                          "country":"USA"
                        }
```
The full_name, site and country is not required.


```bash
DELETE method
```
DELETE request removes the user from the database

For delete user you need:

* http://test.com/api/v1/user/{user_id}
* method DELETE
* Token (exampl: Token 092e00e4893d42cce7dbfddfc3a2f9e9508743d3)


## Like an Dislike posts
Using methods POST, http://test.com/api/v1/like_post/{post_id} or http://test.com/api/v1/dislike_post/{post_id} and Token