from django.urls import path
from .handlers import PostApiHandler, LikeApiHandler, DislikeApiHandler


urlpatterns = [
    path('post', PostApiHandler.as_view(), name='post'),
    path('post/<int:id>', PostApiHandler.as_view()),
    path('like_post/<int:post_id>', LikeApiHandler.as_view()),
    path('dislike_post/<int:post_id>', DislikeApiHandler.as_view()),
]