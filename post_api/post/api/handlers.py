from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.contenttypes.models import ContentType

from .serializers import PostSerializer
from authentication.models import User
from post.models import Post, Like, Dislike


class PostApiHandler(APIView):
    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request, id=None):
        if id:
            try:
                post = Post.objects.get(id=id)
                serializer = self.serializer_class(post)
            except ObjectDoesNotExist:
                return Response(
                    {'error': 'This post is not exist!'},
                    status=status.HTTP_400_BAD_REQUEST
                    )
        else:
            posts = Post.objects.all()
            serializer = self.serializer_class(posts, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save(author=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LikeApiHandler(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, post_id):
        try:
            post = Post.objects.get(id=post_id)
        except ObjectDoesNotExist:
            return Response(
                {'error': 'Bad request'},
                status=status.HTTP_400_BAD_REQUEST
                )
        post_model_type = ContentType.objects.get_for_model(post)
        Like.objects.create(
            content_type=post_model_type,
            object_id=post_id,
            user=self.request.user
            )
        return Response({'success': 'like'}, status=status.HTTP_200_OK)
        

class DislikeApiHandler(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, post_id):
        try:
            post = Post.objects.get(id=post_id)
        except ObjectDoesNotExist:
            return Response(
                {'error': 'Bad request'},
                status=status.HTTP_400_BAD_REQUEST
                )
        post_model_type = ContentType.objects.get_for_model(post)
        Dislike.objects.create(
            content_type=post_model_type,
            object_id=post_id,
            user=self.request.user
            )
        return Response({'success': 'dislike'}, status=status.HTTP_200_OK)