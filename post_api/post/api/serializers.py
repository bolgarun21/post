from post.models import Post
from rest_framework import serializers


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = (
            'id',
            'name_author',
            'title',
            'content',
            'total_likes',
            'total_dislikes',
            'created_at'
        )

    def create(self, validated_data):
        post = Post.objects.create(
            author=validated_data['author'],
            title=validated_data['title'],
            content=validated_data['content'],
        )
        return post