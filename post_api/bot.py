import os
import json
import requests
from time import sleep
from essential_generators import DocumentGenerator


with open('config.json') as f:
    data_config = json.load(f)


def login(email, password):
    r = requests.post(
        '{}/auth/token'.format(os.environ.get('url')),
        data={
            'email': email,
            'password': password
        }
    )
    return r.json()['Token']


def main_bot(num_users):
    gen = DocumentGenerator()

    url = 'https://randomuser.me/api/?results={}&password=lower,number,8-12'.format(
        num_users
        )
    r = requests.get(url)
    users_data = r.json()
    for item in users_data['results']:
        email = item['email']
        password = item['login']['password']

        url = '{}/api/v1/user'.format(os.environ.get('url'))
        headers = {'Content-Type': 'application/json'}
        payload = {
            'email': email,
            'password': password
        }
        requests.post(url, data=json.dumps(payload), headers=headers)
        headers = {
            'Authorization': 'Token {}'.format(login(email, password)),
            'Content-Type': 'application/json'
            }

        for x in range(data_config['max_posts_per_user']):
            content = gen.paragraph()
            splitted = content.split()

            url = '{}/api/v1/post'.format(os.environ.get('url'))
            post_payload = {
                        'title': ' '.join(splitted[0:7]),
                        'content': content
                    }
            response = requests.post(
                url,
                data=json.dumps(post_payload),
                headers=headers
                )

            for i in range(data_config['max_likes_per_user']):
                url = '{}/api/v1/like_post/{}'.format(
                    os.environ.get('url'),
                    response.json()['id']
                    )
                requests.post(url, headers=headers)
        sleep(5)

main_bot(data_config['number_of_users'])
